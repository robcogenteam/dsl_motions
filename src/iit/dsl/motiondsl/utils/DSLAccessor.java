package iit.dsl.motiondsl.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.StringInputStream;

import com.google.inject.Injector;

/**
 * This class loads the machinery of the Motion-DSL (package iit.dsl.motiondsl...)
 * and allows to retrieve the corresponding models, given a string with the
 * text of a compliant document (ie the text model)
 * @author Marco Frigerio
 */
public class DSLAccessor {

    private XtextResourceSet resourceSet = null;
    private Resource resource = null;

    public DSLAccessor() {
        new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
        Injector injector = new iit.dsl.motiondsl.MotionDslStandaloneSetup().createInjectorAndDoEMFRegistration();
        resourceSet = injector.getInstance(XtextResourceSet.class);
        resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
    }

    public iit.dsl.motiondsl.motionDsl.Model getModel(String modeltext) throws IOException
    {
        resource = resourceSet.createResource(URI.createURI("dummy:/"+Long.toString(System.nanoTime())+".motdsl"));
        InputStream in = new StringInputStream(modeltext);
        resource.load(in, resourceSet.getLoadOptions());
        List<Resource.Diagnostic> errors = resource.getErrors();
        if(errors.size() > 0) {
            StringBuffer msg = new StringBuffer();
            msg.append("Errors while loading a document of the Motion-DSL:\n");
            msg.append(modeltext + "\n");
            for(Resource.Diagnostic err : errors) {
                msg.append("\n\t " + err.getMessage() + "\n");
            }
            throw new RuntimeException(msg.toString());
        }
        return (iit.dsl.motiondsl.motionDsl.Model) resource.getContents().get(0);
    }

}
