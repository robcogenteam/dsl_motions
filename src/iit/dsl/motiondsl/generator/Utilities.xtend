package iit.dsl.motiondsl.generator

import iit.dsl.motiondsl.motionDsl.Expr
import iit.dsl.motiondsl.motionDsl.FloatLiteral
import iit.dsl.motiondsl.motionDsl.Model
import iit.dsl.motiondsl.motionDsl.Motion
import iit.dsl.motiondsl.motionDsl.MultExpr
import iit.dsl.motiondsl.motionDsl.PrimitiveMotion
import iit.dsl.motiondsl.motionDsl.Rotation
import iit.dsl.motiondsl.motionDsl.Rotx
import iit.dsl.motiondsl.motionDsl.Roty
import iit.dsl.motiondsl.motionDsl.Rotz
import iit.dsl.motiondsl.motionDsl.Translation
import iit.dsl.motiondsl.motionDsl.Trx
import iit.dsl.motiondsl.motionDsl.Try
import iit.dsl.motiondsl.motionDsl.Trz
import iit.dsl.motiondsl.motionDsl.Frame
import iit.dsl.motiondsl.motionDsl.impl.MotionDslFactoryImpl
import iit.dsl.motiondsl.motionDsl.Constant

import java.util.ArrayList
import java.util.List

import org.eclipse.xtext.EcoreUtil2


class Utilities {
    def static Frame getFrameByName(String name, Model model) {
        for(f : model.framesList.items) {
            if(f.name.equals(name)) {
                return f
            }
        }
        return null
    }

	def static List<Translation> getTranslations(Motion motion) {
	    val ret = new ArrayList<Translation>()
	    for(primitive : motion.primitiveMotions) {
	        if(primitive instanceof Translation) {
	            ret.add(primitive as Translation)
	        }
	    }
	    return ret
	}
	def static List<Rotation> getRotations(Motion motion) {
        val ret = new ArrayList<Rotation>()
        for(primitive : motion.primitiveMotions) {
            if(primitive instanceof Rotation) {
                ret.add(primitive as Rotation)
            }
        }
        return ret
    }




    def public static print(Motion m) {
        for(p : m.primitiveMotions) {
            System::out.print(asString(p))
            System::out.print(" ")
        }
        System::out.println()
    }

    def public static dispatch CharSequence asString(Rotx m) '''rotx(«asString(m.arg)»)'''
    def public static dispatch CharSequence asString(Roty m) '''roty(«asString(m.arg)»)'''
    def public static dispatch CharSequence asString(Rotz m) '''rotz(«asString(m.arg)»)'''
    def public static dispatch CharSequence asString(Trx m)  '''trx(«asString(m.arg)»)'''
    def public static dispatch CharSequence asString(Try m)  '''try(«asString(m.arg)»)'''
    def public static dispatch CharSequence asString(Trz m)  '''trz(«asString(m.arg)»)'''

    def public static dispatch CharSequence asString(FloatLiteral f)  '''«FloatUtils::toStrNoSciNotation(f.value)»'''
    def public static dispatch CharSequence asString(Expr e)  '''<expr>'''//'''«texter.asText(e)»'''//

    //private static MotionToTransforms texter = new MotionToTransforms


    def static dispatch FloatLiteral invert(FloatLiteral f) {
        var FloatLiteral newf =  EcoreUtil2::copy(f)
        newf.value = FloatUtils::invert(f.value)
        return newf
    }
    def static dispatch Constant invert(Constant c) {
        var Constant newc = EcoreUtil2::copy(c)
        newc.minus = ! c.minus
        return newc
    }
    def static dispatch Expr invert(Expr expr) {
        var Expr newExpr = EcoreUtil2::copy(expr)
        newExpr.identifier.minus = !expr.identifier.minus //the actual inversion
        return newExpr
    }
    // Special case for multiplication expressions : invert the float,
    //  multiplier not the literal
    def static dispatch MultExpr invert(MultExpr expr) {
        var MultExpr newExpr = EcoreUtil2::copy(expr)
        newExpr.mult = FloatUtils::invert(expr.mult)
        return newExpr
    }

    def public static MotionSequenceConvention getModelConvention(Model doc) {
        val conventionStr = doc.convention
        if(conventionStr.equals(MotionSequenceConvention::LOCAL.getDocKeyword)) {
            return MotionSequenceConvention::LOCAL
        } else if(conventionStr.equals(MotionSequenceConvention::FIXED.getDocKeyword)) {
            return MotionSequenceConvention::FIXED
        }
        throw new RuntimeException(
        "Unknown value of the feature 'convention' for the model " + doc.name)
    }

    def public static PrimitiveMotion reverse(PrimitiveMotion original) {
        var PrimitiveMotion reversed = EcoreUtil2::copy(original)
        reversed.arg = invert(original.arg)
        return reversed
    }

    def public static Motion reverse(Motion original) {
        val convention = getModelConvention(original.eContainer as Model)
        if(convention == MotionSequenceConvention::FIXED) {
            throw new RuntimeException("Finding the inverse motion " +
                "specification when the model convention is 'fixed' is not " +
                "supported yet"
            )
        }

        val reversed = EcoreUtil2::copy(original)

        reversed.start = original.end
        reversed.end   = original.start

        val originalPrimitives = original.primitiveMotions
        val reversedPrimitives = new ArrayList<PrimitiveMotion>(originalPrimitives.size)
        for(prim : originalPrimitives.reverseView) {
            reversedPrimitives.add( reverse(prim) )
        }

        reversed.primitiveMotions.clear()
        reversed.primitiveMotions.addAll( reversedPrimitives )
        return reversed
    }

    def public static boolean motionsAreSequential(List<Motion> list) {
        if(list == null) return true
        if(list.size() <= 1) return true

        var Frame end = list.head.end
        for(m : list.tail) {
            if( ! end.name.equals(m.start.name) ) {
                return false
            }
            end = m.end
        }
        return true
    }

    def public static Motion compose(List<Motion> list) {
        if(list == null) {
            return null
        }
        if(list.size() == 0) {
            return null
        }
        if( ! motionsAreSequential(list) ) {
            throw(new RuntimeException("Cannot compose a list of motions that are not sequential"))
        }
        val ret = MotionDslFactoryImpl::init().createMotion()

        ret.start = list.head.start
        ret.end   = list.last.end

        var PrimitiveMotion tmpPM = null
        for(motion : list) {
            for (primitiveMotion : motion.primitiveMotions) {
                tmpPM = EcoreUtil2::copy(primitiveMotion)
                ret.primitiveMotions.add(tmpPM)
            }
        }

        ret.setUserName(ret.start.name + "-->" + ret.end.name)

        return ret
    }
}
