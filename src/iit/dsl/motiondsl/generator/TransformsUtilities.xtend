package iit.dsl.motiondsl.generator

import iit.dsl.motiondsl.motionDsl.Model
import iit.dsl.motiondsl.motionDsl.Frame
import iit.dsl.motiondsl.motionDsl.Motion
import iit.dsl.motiondsl.motionDsl.impl.MotionDslFactoryImpl


/**
 * A simple container of two reference frames, interpreted as the start and the
 * end pose of a rigid motion.
 * Instances of this class can be used as placeholders for rigid motions.
 */
class MotionEndPoints {
    Frame start
    Frame end
    new(Frame s, Frame e) {
        start = s
        end   = e
    }
    def public Frame getStart() { return start }
    def public Frame getEnd()   { return end   }
}


/**
 * A simple container of two reference frames, interpreted as a specification
 * of a coordinate transform.
 * In the transform 'A_X_B', 'A' is the left-frame, 'B' is the right-frame.
 * Instances of this class can be used as placeholders for coordinate
 * transformation matrices.
 */
class CoordinateTransformPlaceholder {
    Frame left
    Frame right
    new(Frame l, Frame r) {
        left  = l
        right = r
    }
    def public Frame getLeftFrame()   { return left  }
    def public Frame getRightFrame()  { return right }

    def public static createInstance(iit.dsl.transspecs.transSpecs.FramePair transform)
    {
        val factory = MotionDslFactoryImpl::init()
        val left  = factory.createFrame()
        val right = factory.createFrame()
        left.setName(transform.base.name)
        right.setName(transform.target.name)

        return new CoordinateTransformPlaceholder(left, right)
    }
}


/**
 * This class encapsulates the logic to turn a specification of a coordinate
 * transform, into a specification of a rigid motion, in the form
 * of a MotionEndPoints object.
 *
 * The mapping can actually be done only in two ways, which are implemented
 * in two concrete subclasses of this abstract class.
 * Basically, a transform like A_X_B (transforms coordinates of frame B into
 * coordinates of frame A), can be computed from the information about the rigid
 * motion A --> B or B --> A
 */
abstract class TransformSpecToMotionEndPoints {
    def public MotionEndPoints getMotionEndPoints(
        Model myModel,
        CoordinateTransformPlaceholder transform)
    {
        var Frame start
        var Frame end

        start = Utilities::getFrameByName(transform.leftFrame.name, myModel)
        if(start == null) {
            System::err.println("Warning - could not find frame " + transform.leftFrame.name)
            //TODO log warning
            return null
        }
        end = Utilities::getFrameByName(transform.rightFrame.name, myModel)
        if(end == null) {
            System::err.println("Warning - could not find frame " + transform.rightFrame.name)
            //TODO log warning
            return null
        }
        return new MotionEndPoints(start, end)
    }
}

/**
 * Concrete implementation of TransformSpecToMotionEndPoints, which, given a
 * transform A_X_B returns the rigid motion A-->B
 */
class BaseToTarget extends TransformSpecToMotionEndPoints
{
    override public MotionEndPoints getMotionEndPoints(
        Model model,
        CoordinateTransformPlaceholder transform)
    {
        return super.getMotionEndPoints(model, transform)
    }
}
/**
 * Concrete implementation of TransformSpecToMotionEndPoints, which, given a
 * transform A_X_B returns the rigid motion B-->A
 */
class TargetToBase extends TransformSpecToMotionEndPoints
{
    override public MotionEndPoints getMotionEndPoints(
        Model model,
        CoordinateTransformPlaceholder transform)
    {
        val baseToTarget = super.getMotionEndPoints(model, transform)
        if(baseToTarget == null) return null

        return new MotionEndPoints(baseToTarget.end, baseToTarget.start)
    }
}

/**
 * Utilities to support the code generation for coordinate transforms
 */
class TransformsUtilities {

    new(Model mod) {
        model = mod
        inspector = new ConnectedFramesInspector(model)
    }

    /**
     * Creates the representation of the actual rigid-motion that can be used to
     * determine the given coordinate transformation matrix.
     * The method checks whether such a motion can be inferred from those
     * available in the model this instance was created with. If yes, such
     * motions are composed together and returned as a single object
     * @param transform the specification of the desired coordinate transform
     * @param motionEndPointsGetter the strategy to be used to turn the
     *        specification of the coordinate transform into a specification of
     *        the rigid motion
     * @return a rigid-motion object that has all the information required to
     *         compute the given transformation matrix. Null if such motion does
     *         not exist or cannot be inferred from the current motions model
     */
    def public Motion getMotion(
        CoordinateTransformPlaceholder transform,
        TransformSpecToMotionEndPoints motionEndPointsGetter)
    {
        val pair = motionEndPointsGetter.getMotionEndPoints(model, transform)
        if(pair == null) {
            //TODO log warning
            System::err.println("Warning - could not find reference frames with name "
                + transform.leftFrame.name + " and " + transform.rightFrame.name)
            return null
        }

        val tmpMotions = inspector.getMotions(pair.start, pair.end)
        if( tmpMotions == null ) {
            //TODO log warning
            System::err.println("Warning - These frames are not connected: "
                + pair.start + " and " + pair.end)
            return null
        }

        return Utilities::compose(tmpMotions)
    }

    private Model model = null
    private ConnectedFramesInspector inspector = null
}