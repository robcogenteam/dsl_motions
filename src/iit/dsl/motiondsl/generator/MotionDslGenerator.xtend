package iit.dsl.motiondsl.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator2
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IFileSystemAccess2

import iit.dsl.motiondsl.motionDsl.Model
import org.eclipse.xtext.generator.IGeneratorContext

class MotionDslGenerator implements IGenerator2
{

    override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
        val model = resource.contents.head as Model
        generateCTDSLFiles(model, fsa)
    }
    override afterGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
    }
    override beforeGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
    }


    /**
     * The file name that will be used when generating a document of the
     * Transforms DSL.
     */
    def public CTDSLDocumentName(Model model,
        iit.dsl.coord.generator.Utilities$MatrixConvention convention)
    {
        return model.name + "_" + convention.docKeyword + ".ctdsl"
    }
    def public CTDSLDocumentName(Model model)
    {
        return model.name + ".ctdsl"
    }

    /**
     * Generates a document (i.e. a text file) of the Transforms DSL
     * @param motionsModel the Motion-DSL model containing the specifications
     *        of the rigid motions to be turned into specifications of
     *        coordinate transforms
     * @param fileWriter the file-system-access to be used to generate the file
     * @param convention the convention to be used in the generated document
     * @desiredTransforms the specification of the desired transformation
     *                    matrices to be generated. Optional argument. If null,
     *                    only the default transforms will be generated.
     */
    def public generateCTDSLDocument(
        Model motionsModel,
        IFileSystemAccess fileWriter,
        iit.dsl.coord.generator.Utilities$MatrixConvention convention,
        iit.dsl.transspecs.transSpecs.DesiredTransforms desiredTransforms)
    {
        var CharSequence content
        var String fileName
        var iit.dsl.coord.generator.Utilities$MatrixConvention localConvention

        if(convention == null) {
            fileName = CTDSLDocumentName(motionsModel)
            localConvention = defaultConvention
        } else {
            fileName = CTDSLDocumentName(motionsModel, convention)
            localConvention = convention
        }

        if(desiredTransforms != null) {
            content = gen.generateDSLDoc(motionsModel, localConvention, desiredTransforms)
        } else {
            content = gen.generateDSLDoc(motionsModel, localConvention)
        }
        fileWriter.generateFile(fileName, content)
    }

    def public generateCTDSLDocument(
        Model motionsModel,
        IFileSystemAccess fileWriter,
        iit.dsl.coord.generator.Utilities$MatrixConvention convention)
    {
        generateCTDSLDocument(motionsModel, fileWriter, convention, null)
    }

    def public generateCTDSLDocument(
        Model motionsModel,
        IFileSystemAccess fileWriter,
        iit.dsl.transspecs.transSpecs.DesiredTransforms desiredTransforms)
    {
        generateCTDSLDocument(motionsModel, fileWriter, null, desiredTransforms)
    }

    def public generateCTDSLDocument(
        Model motionsModel,
        IFileSystemAccess fileWriter)
    {
        generateCTDSLDocument(motionsModel, fileWriter, null, null)
    }

    def public generateCTDSLFiles(Model model, IFileSystemAccess fsa)
    {
        generateCTDSLDocument(model, fsa, conventionRight, null)
        generateCTDSLDocument(model, fsa, conventionLeft , null)
    }


/*
    def temp(Model model) {
        val connectivity = new ConnectedFramesInspector(model)


        val fr1 = Utilities::getFrameByName("fr_base", model)
        val fr2 = Utilities::getFrameByName("fr_lowerleg", model)
        if(fr1 != null  &&  fr2 != null) {

            val list = connectivity.getMotions(fr2, fr1)

            for(m : list) {
                for(pm : m.primitiveMotions) {
                    System::out.print(Utilities::asString(pm))
                    System::out.print(" ")
                }
                //System::out.println()
            }
        }
    }
*/
    private static iit.dsl.coord.generator.Utilities$MatrixConvention
        conventionRight = iit::dsl::coord::generator::Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_RIGHT
    private static iit.dsl.coord.generator.Utilities$MatrixConvention
        conventionLeft  = iit::dsl::coord::generator::Utilities$MatrixConvention::TRANSFORMED_FRAME_ON_THE_LEFT
    public static iit.dsl.coord.generator.Utilities$MatrixConvention
        defaultConvention = conventionRight;

    private CoordTransDSL_generator gen = new CoordTransDSL_generator()
}
