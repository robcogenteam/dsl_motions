package iit.dsl.motiondsl.generator;

public enum MotionSequenceConvention {
        LOCAL("local"),
        FIXED("fixed");

        private final String docKeyword;
        private MotionSequenceConvention(String key) { docKeyword = key; }
        /**
         * Return the keyword of the Rigid Motion DSL that corresponds
         * to this instance
         */
        public String getDocKeyword() { return docKeyword; }

}
