package iit.dsl.motiondsl.generator

import iit.dsl.motiondsl.motionDsl.Model
import iit.dsl.motiondsl.motionDsl.Trx
import iit.dsl.motiondsl.motionDsl.Try
import iit.dsl.motiondsl.motionDsl.Trz
import iit.dsl.motiondsl.motionDsl.Rotx
import iit.dsl.motiondsl.motionDsl.Rotz
import iit.dsl.motiondsl.motionDsl.Roty
import iit.dsl.motiondsl.motionDsl.PrimitiveMotion
import iit.dsl.motiondsl.motionDsl.Motion
import iit.dsl.motiondsl.motionDsl.FloatLiteral
import iit.dsl.motiondsl.motionDsl.PlainExpr
import iit.dsl.motiondsl.motionDsl.MultExpr
import iit.dsl.motiondsl.motionDsl.DivExpr
import iit.dsl.motiondsl.motionDsl.Variable
import iit.dsl.motiondsl.motionDsl.Parameter
import iit.dsl.motiondsl.motionDsl.PILiteral
import iit.dsl.motiondsl.motionDsl.ParametersDeclaration

import iit.dsl.coord.generator.Utilities$MatrixConvention

import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import iit.dsl.motiondsl.motionDsl.Constant

/* Code generation templates to generate an instance document of the
 * Coordinate Transforms DSL.
 */
class CoordTransDSL_generator {

    def generateDSLDoc(Model model, MatrixConvention mxConv)
    '''
        «val tokenizer = getTokenizer(Utilities::getModelConvention(model), mxConv)»

        «preamble(model, mxConv)»

        «FOR motion : model.motions»
            {«motion.start.name»}_X_{«motion.end.name»} = «tokenizer.tokenList(motion)»
        «ENDFOR»
    '''


    def generateDSLDoc(Model model, MatrixConvention mxConv,
        iit.dsl.transspecs.transSpecs.DesiredTransforms desiredTransforms)
    '''
        «val tokenizer = getTokenizer(Utilities::getModelConvention(model), mxConv)»
        «val transformToMotion = new BaseToTarget()»
        «val transformsUtils = new TransformsUtilities(model)»

        «preamble(model, mxConv)»

        «FOR tf : desiredTransforms.transforms.specs»
            «val transform = CoordinateTransformPlaceholder::createInstance(tf)»
            «val motion = transformsUtils.getMotion(transform, transformToMotion)»
            «IF motion != null»
                {«tf.base.name»}_X_{«tf.target.name»} = «tokenizer.tokenList(motion)»
            «ENDIF»
        «ENDFOR»
    '''





    def private preamble(Model model, MatrixConvention mxConv) '''
        Model «model.name»

        Frames {
            «FOR fr : model.framesList.items SEPARATOR ', '»«fr.name»«ENDFOR»
        }

        «FOR gr : model.paramGroups»
            «NodeModelUtils::findActualNodeFor(gr).getText()»
        «ENDFOR»

        TransformedFramePos = «mxConv.docKeyword»
    '''




    def private MotionToTransforms getTokenizer(MotionSequenceConvention seqConv,
                            MatrixConvention mxConv)
    {
        if(seqConv.equals(MotionSequenceConvention::FIXED)) {
            if(mxConv.equals(MatrixConvention::TRANSFORMED_FRAME_ON_THE_LEFT)) {
                return new FixedLeft
            } else {
                return new FixedRight
            }
        } else {
            if(mxConv.equals(MatrixConvention::TRANSFORMED_FRAME_ON_THE_LEFT)) {
                return new LocalLeft
            } else {
                return new LocalRight
            }
        }
    }

}

/**
 * Utilities to support the generation of documents of the Transforms-DSL
 */
abstract class MotionToTransforms {
    /**
     * The text compliant with the Transforms-DSL, which contains the
     * specification of the coordinate transform corresponding to the given
     * rigid motion.
     *
     * Specifically, given a motion in the form 'A-->B', this function computes
     * the sequence of primitive transforms that result in the composite
     * transform 'A_X_B'.
     *
     * This functionality is the "core business" of the Motions-DSL, which
     * relieves the user from figuring out manually the correct sequence of
     * transforms (see also the Transforms-DSL package).
     * Four concrete subclasses of this class override this function, providing
     * all the four possible implementations; such alternatives arise from the
     * combination of the two possible conventions involved in the process:
     * the convention about the composition of rigid motions and the convention
     * about the primitive transformation matrices (see the Transforms-DSL).
     *
     * @param m the rigid-motion specification (say 'A-->B') to be turned into a
     *          coordinate-transform specification
     * @return the text with the sequence of transforms that define the
     *         transform 'A_X_B', as it would be written inside a valid
     *         document of the Transforms-DSL
     */
    def public tokenList(Motion m) {}

     /* The following must be consistent with the Coordinate-Transforms-DSL syntax */
    def protected dispatch token(Trx t)  '''Tx(«asText(t.arg)»)'''
    def protected dispatch token(Try t)  '''Ty(«asText(t.arg)»)'''
    def protected dispatch token(Trz t)  '''Tz(«asText(t.arg)»)'''
    def protected dispatch token(Rotx r) '''Rx(«asText(r.arg)»)'''
    def protected dispatch token(Roty r) '''Ry(«asText(r.arg)»)'''
    def protected dispatch token(Rotz r) '''Rz(«asText(r.arg)»)'''

    def protected inverseToken(PrimitiveMotion m) {
        token(invertArg(m))
    }

    def protected invertArg(PrimitiveMotion m) {
        m.arg = Utilities::invert(m.arg)
        return m
    }

    // The following 'asText' methods, basically transform-back the objects to
    //  exactly the same text they were created from; since the Transforms-DSL
    //  has the same grammar of the Motion-DSL, such text is compliant
    //  with the language.
    // 'NodeModelUtils::findActualNodeFor(<obj>).getText()' should exactly do
    //  this task, but for some reason I don't understand it does not return a
    //  non-null node.

    def public dispatch CharSequence asText(FloatLiteral f)
        '''«Utilities::asString(f)»'''

    def public dispatch CharSequence asText(Variable  va)
        '''«IF va.minus»-«ENDIF»«va.varname»'''

    def public dispatch CharSequence asText(Constant c)
        '''«IF c.minus»-«ENDIF»consts.«c.name»'''

    def public dispatch CharSequence asText(Parameter pa)
        '''«IF pa.minus»-«ENDIF»«(pa.param.eContainer as ParametersDeclaration).name».«pa.param.name»'''

    def public dispatch CharSequence asText(PILiteral pi)
        '''«IF pi.minus»-«ENDIF»PI'''

    def public dispatch CharSequence asText(PlainExpr expr)
        '''«asText(expr.identifier)»'''

    def public dispatch CharSequence asText(MultExpr expr)
        '''«Float::toString(expr.mult)» «asText(expr.identifier)»'''

    def public dispatch CharSequence asText(DivExpr expr)
        '''«asText(expr.identifier)» / «Float::toString(expr.div)»'''
}


// The concrete implementations of tokenList() :

class LocalRight extends MotionToTransforms {
    override public tokenList(Motion m) '''
        «FOR primitive : m.primitiveMotions SEPARATOR" "»«primitive.token»«ENDFOR»'''
}
class LocalLeft extends MotionToTransforms {
    override public tokenList(Motion m) '''
        «FOR primitive : m.primitiveMotions SEPARATOR" "»«primitive.inverseToken»«ENDFOR»'''
}
class FixedRight extends MotionToTransforms {
    override public tokenList(Motion m) '''
        «val translations = Utilities::getTranslations(m)»
        «val rotations    = Utilities::getRotations(m)»
        «FOR tr : translations SEPARATOR" "»«tr.token»«ENDFOR»«FOR rot : rotations.reverseView SEPARATOR" "»«rot.token»«ENDFOR»'''
}
class FixedLeft extends MotionToTransforms { // TODO check if the logic here is correct!
    override public tokenList(Motion m) '''
        «val translations = Utilities::getTranslations(m)»
        «val rotations    = Utilities::getRotations(m)»
        «FOR tr : translations SEPARATOR" "»«tr.inverseToken»«ENDFOR»«FOR rot : rotations.reverseView SEPARATOR" "»«rot.inverseToken»«ENDFOR»'''
}


