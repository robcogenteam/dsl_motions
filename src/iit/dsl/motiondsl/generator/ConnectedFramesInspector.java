package iit.dsl.motiondsl.generator;

import iit.dsl.motiondsl.motionDsl.Frame;
import iit.dsl.motiondsl.motionDsl.Model;
import iit.dsl.motiondsl.motionDsl.Motion;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 * This class determines whether a path exists between arbitrary Frames,
 * according to the motions specified in a given Model.
 * For example, if a motion exists between frame A and B, and between frame
 * B and C, then the motion from A to C can also be inferred.
 *
 * @author Marco Frigerio
 *
 */
class ConnectedFramesInspector {

    private final Model motionsSet;
    private final Graph<Frame, GraphEdge> graph;
    private final ConnectivityInspector<Frame, GraphEdge> graphConnectivity;

    public ConnectedFramesInspector(Model m) {
        motionsSet = m;
        graph = createGraph(motionsSet);
        graphConnectivity = new ConnectivityInspector<Frame, GraphEdge>(graph);
    }

    /**
     * Tells whether there is a sequence of motions that relate the two given
     * frames
     * @param f1 The first reference frame
     * @param f2 The other reference frame
     * @return true if a list of motions connecting f1 and f2 can be inferred,
     *          false otherwise
     */
    public boolean areConnected(Frame f1, Frame f2) {
        return graphConnectivity.pathExists(f1, f2);
    }

    /**
     * The sequence of motions that bring the first frame into the second one.
     * @param f1 The frame to start from
     * @param f2 The frame to end into
     * @return a list of Motion(s) that specify the rigid movements to apply
     *         to frame f1 in order to make it coincide with f2
     */
    public List<Motion> getMotions(Frame f1, Frame f2) {
        if( ! areConnected(f1, f2) ) {
            return null;
        }
        GraphPath<Frame,GraphEdge> path = DijkstraShortestPath
                    .findPathBetween(graph, f1, f2);

        if( path == null ) {
            throw(new RuntimeException("Cannot find path between frame " +
                    f1.getName() + " and " + f2.getName() + ", even if " +
                    "they appear to be connected"));
        }
        List<GraphEdge> edges = path.getEdgeList();
        List<Motion> ret = new ArrayList<Motion>(edges.size());
        for(GraphEdge edge : edges) {
            ret.add(edge.getMotion());
        }
        return ret;
    }


    private static class GraphEdge {
        private final Motion motion;

        public GraphEdge(Motion compositeMotion) {
            motion = compositeMotion;
        }
        public Motion getMotion() {
            return motion;
        }
        public int getWeight() {
            return motion.getPrimitiveMotions().size();
        }
    }

    private static Graph<Frame, GraphEdge> createGraph(Model model) {
        Graph<Frame, GraphEdge> graph =
                new SimpleDirectedGraph<Frame,
                ConnectedFramesInspector.GraphEdge>(GraphEdge.class);
        Frame v1, v2;
        for(Motion m : model.getMotions()) {
            v1 = m.getStart();
            v2 = m.getEnd();
            graph.addVertex(v1);
            graph.addVertex(v2);
            graph.addEdge(v1, v2, new GraphEdge(m));
            graph.addEdge(v2, v1, new GraphEdge( Utilities.reverse(m) ));
        }
        return graph;
    }


}