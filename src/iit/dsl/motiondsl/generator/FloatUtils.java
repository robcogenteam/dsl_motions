package iit.dsl.motiondsl.generator;

public class FloatUtils {
    public static float invert(float num) {
        return -num;
    }
    public static boolean isZero(float num) {
        return num==0.0;
    }
    public static float mult(float a, float b) {
        return a*b;
    }
    public static float div(float a, float b) {
        return a/b;
    }

    public static String toStrNoSciNotation(Float num) {
        return String.format(java.util.Locale.US,"% 06.5f", num);
    }
}
